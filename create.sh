#!/bin/sh

if [ $# -lt 1 ]; then
	echo "Usage: $0 <name> [<podman/docker args>]"
	exit
fi

NAME="$1"; shift
DOCKER="$(which podman||which docker)"

$DOCKER container create --workdir "$HOME" --userns=keep-id \
	--cap-add=SYS_PTRACE --security-opt="seccomp=unconfined" \
	--name "$NAME" --hostname "$NAME" \
	$([ -n "$WAYLAND_DISPLAY" ] && printf -- "--env WAYLAND_DISPLAY=$WAYLAND_DISPLAY --env TERM  --env XDG_RUNTIME_DIR=/tmp -v $XDG_RUNTIME_DIR/$WAYLAND_DISPLAY:/tmp/$WAYLAND_DISPLAY") \
	$@ \
	registry.gitlab.com/rickra/ctf-docker:master sh -c 'trap : TERM INT; sleep infinity & wait'

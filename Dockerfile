FROM docker://archlinux:base-devel

ENV USERNAME richard

RUN sed -i 's/^#Color$/Color/g' /etc/pacman.conf && \
	pacman -Syu --noconfirm git openssh go && \
	printf "root ALL=(ALL) ALL\n\n$USERNAME ALL=(ALL) NOPASSWD: ALL\n" > /etc/sudoers ; \
	useradd -U --create-home $USERNAME

USER $USERNAME
WORKDIR /home/$USERNAME

RUN \
	git clone https://aur.archlinux.org/paru-bin.git && \
	cd paru-bin && \
	makepkg --noconfirm --syncdeps --install && \
	cd .. && rm -rf paru-bin

# TODO kubecolor is broken
RUN mkdir -p ~/.cache/zsh && \
	git clone https://gitlab.com/rickra/dotfiles.git ~/.config && \
	ln -sf ~/.config/zsh/.zshenv ~/.zshenv && \
	paru -Syu --noconfirm \
		httpie curlie hurl jq \
		gcc \
		man-db man-pages \
		zsh zsh-syntax-highlighting zsh-theme-powerlevel10k-bin-git \
		tmux \
		gdb gef-git \
		strace \
		rizin rz-ghidra \
		neovim vscode-json-languageserver typst-lsp-bin bash-language-server rust-analyzer pyright ccls \
		python-pwntools \
		kubectl \
		ttf-mona-sans ttf-hack-nerd ttf-font-awesome foot

RUN sudo chsh -s /usr/bin/zsh $USERNAME && \
	nvim --headless "+Lazy! update" +qa && \
	sudo sudo mkdir --parents /usr/local/share/zsh/site-functions/ && \
	kubectl completion zsh | sudo tee /usr/local/share/zsh/site-functions/_kubectl > /dev/null
CMD 'zsh'
